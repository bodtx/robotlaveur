// This is the library for the TB6612 that contains the class Motor and all the
// functions
#include <SparkFun_TB6612.h>
#include <Timemark.h>

// Pins for all inputs, keep in mind the PWM defines must be on PWM pins
// the default pins listed are the ones used on the Redbot (ROB-12097) with
// the exception of STBY which the Redbot controls with a physical switch
#define AIN1 8
#define BIN1 12
#define AIN2 3
#define BIN2 13
#define PWMA 9
#define PWMB 11
#define STBY 10
Timemark cleaningDelay(1800000);
Timemark forwardDelay(5000);
// these constants are used to allow you to make your motor configuration
// line up with function names like forward.  Value can be 1 or -1
const int offsetA = 1;
const int offsetB = 1;

// Initializing motors.  The library will allow you to initialize as many
// motors as you have memory for.  If you are using functions like forward
// that take 2 motors as arguements you can either write new functions or
// call the function more than once.
Motor motor1 = Motor(AIN1, AIN2, PWMA, offsetA, STBY);
Motor motor2 = Motor(BIN1, BIN2, PWMB, offsetB, STBY);

// évite de rester coincer en évitant toujours du même côté, au bout de 5 fois le même tampon on tente une évasion pa l'autre côté.
byte countL;
byte countR;

void setup()
{ pinMode(4, INPUT_PULLUP);
  pinMode(5, INPUT_PULLUP);

  forwardDelay.start();
  cleaningDelay.start();//30 minutes de nettoyage
}


void loop()
{
  //TODO optimiser la rapidité de lecture des capteurs avec des else
  if (!cleaningDelay.expired() && cleaningDelay.running()) {
    forward(motor1, motor2, 200);
    //on avance un certain temps car si on est coincé sans que les capteurs aient détecté de choc, on va patiner dans le vide sans fin.
    if (forwardDelay.expired()) {
      if (random(0, 2) == 0)
        dodge('L');
      else
        dodge('R');
    }
    if (digitalRead(4) == LOW) {
      if (countL < 5) {
        dodge('L');
        countL++;
        countR = 0;
      }
      else dodge('R');
    } else if (digitalRead(5) == LOW) {
      if (countR < 5) {
        dodge('R');
        countR++;
        countL = 0;
      }
      else dodge('L');
    }
  }
  else {
    brake(motor1, motor2); //fin du cycle de nettoyage
    motor1.standby();
    motor2.standby();
    forwardDelay.stop();
    cleaningDelay.stop();
  }

}

void dodge(char direction) {
  brake(motor1, motor2);
  delay(500);
  back(motor1, motor2, 180);
  delay(1000);
  switch (direction) {
    case 'L':
      left(motor1, motor2, 350);
      break;
    case 'R':
      right(motor1, motor2, 350);
      break;
  }
  delay(random(300, 1200));
  brake(motor1, motor2);
  delay(500);
  forward(motor1, motor2, 100);
  delay(500);
  forwardDelay.start();


}
